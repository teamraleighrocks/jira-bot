﻿using FluentAssertions;
using JIRABot.JIRA;
using NUnit.Framework;

namespace JIRABot.Tests
{
	[TestFixture]
	public class JiraKeyParsingTests
	{
		[Test]
		public void GetJiraKeys_Should_ReturnEmptyArray_When_EmptyString()
		{
			// Arrange
			string input = string.Empty;

			// Act
			var keys = JiraProvider.GetJiraKeys(input);

			// Assert
			keys.Should().BeEmpty();
		}

		[Test]
		public void GetJiraKeys_Should_ReturnEmptyArray_When_NoKeys()
		{
			// Arrange
			string input = "Those are not valid keys HA - 1 or B- and C1 and even JB1 or NA- 1";

			// Act
			var keys = JiraProvider.GetJiraKeys(input);

			// Assert
			keys.Should().BeEmpty();
		}

		[Test]
		public void GetJiraKeys_Should_ReturnKeys_When_InputHasJiraItems()
		{
			// Arrange
			string input = "This is a valid jira Key: ABC-123 and H-1 or JB-567";
			var expected = new string[] { "ABC-123", "H-1", "JB-567" };

			// Act
			var keys = JiraProvider.GetJiraKeys(input);

			// Assert
			keys.Should().Equal(expected);
		}

	}
}
