﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Net;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using FluentAssertions;
using NUnit.Framework;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Extensions.MonoHttp;

namespace JIRABot.Tests
{
    [Ignore("Methods for testing oauth flow")]
    [TestFixture]
    public class OAuthTests
    {
        [Test]
        public void RestsharpOauth_Should_ThrowException_Because_ItDoesntSupportRSASHA1()
        {
            // Arrange
            const string consumerKey = "OauthKey";
            const string consumerSecret = "MIICXAIBAAKBgQCqqKsNSY0ifgrjik8Lya9jBWaO/1q0tmP+wgopphf9/4zCWrUz39rfhhsiCrg6LiNgXvqD3jKmclR/7NnxRksethwXfrjZixZxJZ0nUl49dwi9Z/TPzJbVfTmlZVbMNa+4RThf0/4ycLwVPh11ZFzmQI6+CNSXJ32FPd1Yk/o8EQIDAQABAoGAQ0edr+5YSsngJxaG1UHc+C/P3/5B1060m2IKOaM2gDd4WcMC/wF4u3iWR9RsDtvTrJGlN8motEAMmiZZQaqRwFPyh1t1bSD5EHu8tAchaLEgwDuq05elJvI8qNJqLRFFjCHfP2r+KpImwvs0jKYD9diHKNKtv6W3KRtgQ7xIDDECQQDW3D08zB7GHLQIgzB2mqX4xpxyeRKpeD6dNiaIu8a6QIw1gml9Q9hVNrf40RYpud8tQ5Qw5oKI1zIzd0YS0ldNAkEAy1XQ8aFXdHL4yIM987K1t8+ila4AOzgMOYnuLjC19r8bqLXmYlMhxp0abgkveboGryF+5c5KpRoVn7PpGjN91QJBANOk5n144+dqZrzr3VU3SL4hhgIM2SEuXR8nNBYEQeE9q98Zsye9KN/nopjp/NblTL2dj+ALcBGzSLQBjXkE0RECQEpJHN/aKQBDLL7cq9qHlLoFool6m10HOfgfeyPm6eOuqCU352ZOps1QAiLnOjAHlX+QBjNg7YZqBDJnYScQXs0CQB3uKIV7mf3rKDc80W3snxt7yclqdQ09vG91Jy498W1Ut+cpiYyAmPrrmcD/JmGDTrB7wI962Kq4+SMCFZeJQ9Y=";
            Uri baseUrl = new Uri("https://raleighrocks.atlassian.net");

            OAuth1Authenticator authenticator = OAuth1Authenticator.ForRequestToken(consumerKey, consumerSecret, "oob");
            authenticator.SignatureMethod = RestSharp.Authenticators.OAuth.OAuthSignatureMethod.RsaSha1;

            RestClient client = new RestClient(baseUrl)
            {
                Authenticator = authenticator
            };

            RestRequest request = new RestRequest("plugins/servlet/oauth/request-token", Method.POST);

            // Act
            IRestResponse response = client.Execute(request);

            Assert.NotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            NameValueCollection qs = HttpUtility.ParseQueryString(response.Content);
            string oauthToken = qs["oauth_token"];
            string oauthTokenSecret = qs["oauth_token_secret"];

            Assert.NotNull(oauthToken);
            Assert.NotNull(oauthTokenSecret);

            request = new RestRequest("plugins/servlet/oauth/authorize");
            request.AddParameter("oauth_token", oauthToken);

            string url = client.BuildUri(request).ToString();
            Process.Start(url);

            string verifier = "123456"; // <-- Breakpoint here (set verifier in debugger)

            request = new RestRequest("plugins/servlet/oauth/access-token", Method.POST);
            var oauth1Authenticator = OAuth1Authenticator.ForAccessToken(consumerKey, consumerSecret, oauthToken,
                oauthTokenSecret, verifier);
            oauth1Authenticator.SignatureMethod = RestSharp.Authenticators.OAuth.OAuthSignatureMethod.RsaSha1;
            client.Authenticator = oauth1Authenticator;
            response = client.Execute(request);

            Assert.NotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            qs = HttpUtility.ParseQueryString(response.Content);
            oauthToken = qs["oauth_token"];
            oauthTokenSecret = qs["oauth_token_secret"];

            Assert.NotNull(oauthToken);
            Assert.NotNull(oauthTokenSecret);

            request = new RestRequest("rest/api/2/issue/JB-1");
            client.Authenticator = OAuth1Authenticator.ForProtectedResource(consumerKey, consumerSecret, oauthToken,
                                                                            oauthTokenSecret, RestSharp.Authenticators.OAuth.OAuthSignatureMethod.RsaSha1);

            response = client.Execute(request);

            Assert.NotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void SimpleOauthLibrary_Should_GetRequestToken()
        {
            // Arrange
            const string consumerKey = "OauthKey";
            const string consumerSecret = "MIICXAIBAAKBgQCqqKsNSY0ifgrjik8Lya9jBWaO/1q0tmP+wgopphf9/4zCWrUz39rfhhsiCrg6LiNgXvqD3jKmclR/7NnxRksethwXfrjZixZxJZ0nUl49dwi9Z/TPzJbVfTmlZVbMNa+4RThf0/4ycLwVPh11ZFzmQI6+CNSXJ32FPd1Yk/o8EQIDAQABAoGAQ0edr+5YSsngJxaG1UHc+C/P3/5B1060m2IKOaM2gDd4WcMC/wF4u3iWR9RsDtvTrJGlN8motEAMmiZZQaqRwFPyh1t1bSD5EHu8tAchaLEgwDuq05elJvI8qNJqLRFFjCHfP2r+KpImwvs0jKYD9diHKNKtv6W3KRtgQ7xIDDECQQDW3D08zB7GHLQIgzB2mqX4xpxyeRKpeD6dNiaIu8a6QIw1gml9Q9hVNrf40RYpud8tQ5Qw5oKI1zIzd0YS0ldNAkEAy1XQ8aFXdHL4yIM987K1t8+ila4AOzgMOYnuLjC19r8bqLXmYlMhxp0abgkveboGryF+5c5KpRoVn7PpGjN91QJBANOk5n144+dqZrzr3VU3SL4hhgIM2SEuXR8nNBYEQeE9q98Zsye9KN/nopjp/NblTL2dj+ALcBGzSLQBjXkE0RECQEpJHN/aKQBDLL7cq9qHlLoFool6m10HOfgfeyPm6eOuqCU352ZOps1QAiLnOjAHlX+QBjNg7YZqBDJnYScQXs0CQB3uKIV7mf3rKDc80W3snxt7yclqdQ09vG91Jy498W1Ut+cpiYyAmPrrmcD/JmGDTrB7wI962Kq4+SMCFZeJQ9Y=";

            var provider = JIRABot.JIRA.OAuth.Crypto.DecodeRsaPrivateKey(Convert.FromBase64String(consumerSecret.Replace("\n", string.Empty)));

            string requestUrl = "https://raleighrocks.atlassian.net/plugins/servlet/oauth/request-token";
            string userAuthorizeUrl = "https://raleighrocks.atlassian.net/plugins/servlet/oauth/authorize";
            string accessUrl = "https://raleighrocks.atlassian.net/plugins/servlet/oauth/access-token";

            var consumerContext = new OAuthConsumerContext
            {
                ConsumerKey = consumerKey,
                SignatureMethod = SignatureMethod.RsaSha1,
                Key = provider
            };

            // Act
            var session = new OAuthSession(consumerContext, requestUrl, userAuthorizeUrl, accessUrl);

            // get a request token from the provider
            var requestToken = session.GetRequestToken("POST");

            // generate a user authorize url for this token (which you can use in a redirect from the current site)
            // e.g. https://raleighrocks.atlassian.net/plugins/servlet/oauth/authorize?oauth_token=A5BMVnxTY8RnLPRmrvcHODWayDwaEIZL
            string authorizationLink = session.GetUserAuthorizationUrlForToken(requestToken);

            Process.Start(authorizationLink);

            var answer = Console.ReadLine();
            // breakpoint here and change the answer value

            IToken accessToken = session.ExchangeRequestTokenForAccessToken(requestToken, "POST", answer);

            var response = session.Request().Get().ForUrl("https://raleighrocks.atlassian.net/rest/api/2/issue/JB-1").ToString();

            // Assert
            authorizationLink.Should().NotBeNull();
        }
    }
}
