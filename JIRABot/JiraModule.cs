﻿using System;
using Autofac;
using JIRABot.Dialogs;
using JIRABot.JIRA;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Internals.Fibers;
using Microsoft.Bot.Builder.Luis;

namespace JIRABot
{
    public class JiraModule : Module
    {
        // todo: add all dependencies
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(c => new Microsoft.Bot.Builder.Luis.LuisModelAttribute("21469e40-5a20-4e8b-9415-3d7c1b1f9021", "4b1d0c291f5747cea115d30926304480")).AsSelf().AsImplementedInterfaces().SingleInstance();

            // register the top level dialog
            //builder.RegisterType<RootDialog>().As<IDialog<object>>().InstancePerDependency();
            builder.RegisterType<JiraDialog>().As<IDialog<object>>().InstancePerDependency();

            // register other dialogs we use
            builder.Register((c, p) => new LoginDialog(c.Resolve<IJiraProvider>())).AsSelf().InstancePerDependency();
            builder.RegisterType<LuisService>().Keyed<ILuisService>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().SingleInstance();

            //builder.Register((c, p) => new LoginDialog()).As<IDialog<object>>().InstancePerDependency();
            //builder.Register((c, p) => new ShowJiraDialog(c.Resolve<IJiraProvider>())).AsSelf().InstancePerDependency();

            // register some singleton services
            //builder.RegisterType<LuisService>().Keyed<ILuisService>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<JiraProvider>().Keyed<IJiraProvider>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().SingleInstance();

            // register some objects dependent on the incoming message
            //builder.Register(c => new RenderingAlarmService(new AlarmService(c.Resolve<IAlarmScheduler>(), c.Resolve<ConversationReference>()), c.Resolve<Func<IAlarmRenderer>>(), c.Resolve<IBotToUser>(), c.Resolve<IClock>())).Keyed<IAlarmService>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().InstancePerMatchingLifetimeScope(DialogModule.LifetimeScopeTag);
            //builder.RegisterType<AlarmScorable>().Keyed<AlarmScorable>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().InstancePerMatchingLifetimeScope(DialogModule.LifetimeScopeTag);
            //builder.RegisterType<AlarmRenderer>().Keyed<IAlarmRenderer>(FiberModule.Key_DoNotSerialize).AsImplementedInterfaces().InstancePerMatchingLifetimeScope(DialogModule.LifetimeScopeTag);
        }
    }
}
