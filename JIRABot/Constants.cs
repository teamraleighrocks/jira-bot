﻿namespace JIRABot
{
    internal class UserDataKeys
    {
        public const string NicknameKey = "Nickname";
        public const string OAuthTokenKey = "OAuthToken";
        public const string OAuthTokenSecretKey = "OAuthTokenSecret";
    }

    internal class IvrOptions
    {
        internal const string WelcomeMessage = "Hi, you have reached JIRA bot.";

        internal const string MainMenuPrompt =
           "If you want to create a new JIRA item press 1, to hear more about this bot press 2. To repeat the options press 3.";

        internal const string NewJiraItemPrompt =
            "To create a task item press 1, to create an improvement item press 2, to create a new feature item press 3, to create a bug item press 4, to create an epic item press 5, to create an investigation item press 6. Press the hash key to return to the main menu";

        internal const string MoreInfoPrompt = "JIRA bot is awesome!";

        internal const string Ending = "Thank you for using JIRA bot. Have a good one!";
    }
}