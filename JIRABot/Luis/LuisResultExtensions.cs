﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Bot.Builder.Luis.Models;

namespace JIRABot.Luis
{
    public static class LuisResultExtensions
    {
        /// <summary>
        /// Gets the JIRA item type entity
        /// </summary>
        internal static EntityRecommendation GetEntity(this LuisResult luisResult, string type)
        {
            return luisResult.Entities.FirstOrDefault(e => e.Type.StartsWith(type));
        }

        /// <summary>
        /// Gets max entity index
        /// </summary>
        internal static int GetMaxEntityIndex(this LuisResult luisResult, string type, int currentIndex)
        {
            string entity = luisResult.GetEntity(type).Entity;
            return Math.Max(luisResult.Query.IndexOf(entity, StringComparison.OrdinalIgnoreCase) + entity.Length, currentIndex);
        }

        /// <summary>
        /// Gets the JIRA item type based on the entities from LUIS
        /// </summary>
        /// <returns>The proper JIRA type name, or null if nothing found/matches</returns>
        internal static string GetItemType(this LuisResult luisResult)
        {
            var entity = luisResult.GetEntity(LuisConstants.JiraItemType);
            return entity != null
                ? Entities.JiraItemTypes.All.FirstOrDefault(i => i.Key == entity.Type)?.Text
                : null;
        }

        /// <summary>
        /// Gets the JIRA item priority based on the entities from LUIS
        /// </summary>
        /// <returns>The proper JIRA priority, or null if nothing found/matches</returns>
        internal static string GetItemPriority(this LuisResult luisResult)
        {
            var entity = luisResult.GetEntity(LuisConstants.JiraItemPriority);
            return entity != null
                ? Entities.JiraItemPriorities.All.FirstOrDefault(i => i.Key == entity.Type || string.Equals(i.Code, entity.Entity, StringComparison.OrdinalIgnoreCase))?.Text
                : null;
        }

        /// <summary>
        /// Gets the JIRA item summary from luis result.
        /// </summary>
        /// <returns>The summary of a new JIRA item</returns>
        internal static string GetSummary(this LuisResult luisResult, int startIndex)
        {
            if (luisResult.Query.Length <= startIndex)
                return string.Empty;

            return luisResult.Query.Substring(startIndex);
        }

        /// <summary>
        /// Given input from LUIS, based on entities present that represent different issue statuses, return the JIRA-equivalents
        /// </summary>
        /// <returns>JIRA-equivalent statuses to be used in a JQL query, or an empty list if no LUIS entities are found.</returns>
        internal static List<string> GetJiraStatus(this LuisResult result)
        {
            var itemFilters = new List<string>();
            if (result.Entities.Any(e => e.Type == LuisConstants.JiraStateComplete))
            {
                itemFilters.Add("Done");
                itemFilters.Add("Resolved");
                itemFilters.Add("Closed");
            }

            if (result.Entities.Any(e => e.Type == LuisConstants.JiraStateInProgress))
            {
                itemFilters.Add("In Progress");
            }

            if (result.Entities.Any(e => e.Type == LuisConstants.JiraStateNotStarted))
            {
                itemFilters.Add("Open");
                itemFilters.Add("To Do");
            }
            return itemFilters;
        }

        /// <summary>
        /// Gets items based on entities in the result
        /// </summary>
        internal static string[] ExtractJiraItems(this LuisResult result)
        {
            return
                result.Entities.Where(e => e.Type == "JiraItem")
                    .Select(e => new StringBuilder(e.Entity).Replace(" ", "").ToString().ToUpper())
                    .ToArray();
        }
    }
}