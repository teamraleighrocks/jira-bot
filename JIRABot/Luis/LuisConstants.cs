﻿namespace JIRABot.Luis
{
    public static class LuisConstants
    {
        public const string JiraStateComplete = "JiraStateComplete";
        public const string JiraStateNotStarted = "JiraStateNotStarted";
        public const string JiraStateInProgress = "JiraStateInProgress";

        public const string JiraItemType = "JiraItemType";
        public const string JiraItemPriority = "JiraItemPriority";
    }
}