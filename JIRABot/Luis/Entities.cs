﻿namespace JIRABot.Luis
{
    public static class Entities
    {
        public class Item
        {
            public string Key { get; }
            public string Text { get; }
            public string Code { get; }

            public Item(string type, string key, string text = null, string code = null)
            {
                Key = type + "::" + key;
                Text = text ?? key;
                Code = code;
            }
        }

        public class ItemBuilder
        {
            private readonly string _itemType;
            private readonly string _codeBase;

            public ItemBuilder(string itemType, string codeBase = null)
            {
                _itemType = itemType;
                _codeBase = codeBase;
            }

            public Item CreateItem(string key, string text = null, int? code = null)
            {
                return new Item(_itemType, key, text, _codeBase + code);
            }
        }

        public class JiraItemTypes
        {
            private static readonly ItemBuilder _itemBuilder = new ItemBuilder(LuisConstants.JiraItemType);

            public static readonly Item Bug = _itemBuilder.CreateItem("Bug");
            public static readonly Item Epic = _itemBuilder.CreateItem("Epic");
            public static readonly Item Improvement = _itemBuilder.CreateItem("Improvement");
            public static readonly Item Investigation = _itemBuilder.CreateItem("Investigation");
            public static readonly Item NewFeature = _itemBuilder.CreateItem("NewFeature", "New Feature");
            public static readonly Item Task = _itemBuilder.CreateItem("Task");

            public static readonly Item[] All = { Bug, Epic, Improvement, Investigation, NewFeature, Task };
        }

        public class JiraItemPriorities
        {
            private static readonly ItemBuilder _itemBuilder = new ItemBuilder(LuisConstants.JiraItemPriority, "P");

            public static readonly Item Lowest = _itemBuilder.CreateItem("Lowest", code: 5);
            public static readonly Item Low = _itemBuilder.CreateItem("Low", code: 4);
            public static readonly Item Medium = _itemBuilder.CreateItem("Medium", code: 3);
            public static readonly Item High = _itemBuilder.CreateItem("High", code: 2);
            public static readonly Item Highest = _itemBuilder.CreateItem("Highest", code: 1);

            public static readonly Item[] All = { Lowest, Low, Medium, High, Highest };
        }
    }
}