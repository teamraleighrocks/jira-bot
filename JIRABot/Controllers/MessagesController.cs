﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Autofac;
using JIRABot.Dialogs;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Connector;
using System.Linq;
using System;

namespace JIRABot
{
    // Autofac provides a mechanism to inject ActionFilterAttributes from the container
    // but seems to require the implementation of special interfaces
    // https://github.com/autofac/Autofac.WebApi/issues/6
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        public async Task<HttpResponseMessage> Post([FromBody] Activity activity, System.Threading.CancellationToken token)
        {
            if (activity != null)
            {
                if (activity.Type == ActivityTypes.Message)
                {
                    using (var scope = DialogModule.BeginLifetimeScope(Conversation.Container, activity))
                    {
                        await Conversation.SendAsync(activity, () => new ExceptionHandlerDialog<object>(scope.Resolve<IDialog<object>>()));
                    }
                }
                else
                {
                    await HandleSystemMessage(activity);
                }

            }

            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }

        private async Task<Activity> HandleSystemMessage(Activity message)
        {
            if (message.Type == ActivityTypes.DeleteUserData)
            {
                // Implement user deletion here
                // If we handle user deletion, return a real message
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels

                if (message.MembersAdded != null && message.MembersAdded.Any())
                {
                    await ReplyAsync(message, Messages.Greeting);
                }
                else if (message.MembersRemoved != null && message.MembersRemoved.Any())
                {
                    await ReplyAsync(message, Messages.Goodbye);
                }
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened

                if (message.Action == "add")
                {
                    await ReplyAsync(message, Messages.Greeting);
                }
                else if (message.Action == "remove")
                {
                    await ReplyAsync(message, Messages.GoodbyeBot);
                }
            }
            else if (message.Type == ActivityTypes.Typing)
            {
                // Handle knowing tha the user is typing
            }
            else if (message.Type == ActivityTypes.Ping)
            {
            }

            return null;
        }

        private async Task<ResourceResponse> ReplyAsync(Activity message, string text)
        {
            var reply = message.CreateReply(text);
            ConnectorClient connector = new ConnectorClient(new Uri(message.ServiceUrl));
            return await connector.Conversations.ReplyToActivityAsync(reply);
        }
    }
}