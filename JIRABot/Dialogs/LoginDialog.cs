﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Internals.Fibers;
using Microsoft.Bot.Connector;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Extensions.MonoHttp;

namespace JIRABot
{
    [Serializable]
    public class LoginDialog : IDialog<object>
    {
        readonly IJiraProvider _jiraProvider;
        readonly string url, consumerKey, consumerSecret;

        public LoginDialog(IJiraProvider jiraProvider)
        {
            url = BotConfiguration.JiraUrl;
            consumerKey = BotConfiguration.OAuthConsumerKey;
            consumerSecret = BotConfiguration.OAuthConsumerSecret;
            SetField.NotNull(out _jiraProvider, nameof(_jiraProvider), jiraProvider);
        }

        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Performs OAuth authentication
        /// </summary>
        /// <returns>The received async.</returns>
        /// <param name="context">Context.</param>
        /// <param name="result">Result.</param>
        /// <see href="https://developer.atlassian.com/cloud/jira/platform/jira-rest-api-oauth-authentication/"/>
        /// <see href="https://github.com/restsharp/RestSharp/blob/master/RestSharp.IntegrationTests/oAuth1Tests.cs"/>
        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            try
            {
                var activity = await result as Activity;

                OAuth1Authenticator authenticator = OAuth1Authenticator.ForRequestToken(consumerKey, consumerSecret, "oob");
                authenticator.SignatureMethod = RestSharp.Authenticators.OAuth.OAuthSignatureMethod.RsaSha1;

                RestClient client = new RestClient(url)
                {
                    Authenticator = authenticator
                };

                RestRequest request = new RestRequest(JiraProvider.OAuthJiraRequestUrl, Method.POST);

                IRestResponse response = await client.ExecutePostTaskAsync(request);

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    context.Fail(new InvalidOperationException($"Could not get request access token from {request.Resource}. Response: {response.Content}"));
                }

                NameValueCollection qs = HttpUtility.ParseQueryString(response.Content);
                string oauthToken = qs["oauth_token"];
                string oauthTokenSecret = qs["oauth_token_secret"];

                context.UserData.SetValue(UserDataKeys.OAuthTokenKey, oauthToken);
                context.UserData.SetValue(UserDataKeys.OAuthTokenSecretKey, oauthTokenSecret);

                request = new RestRequest(JiraProvider.OAuthUserAuthorizeUrl);
                request.AddParameter("oauth_token", oauthToken);

                string authorizationLink = client.BuildUri(request).ToString();

                await ShowSignInCard(context, activity, authorizationLink);

                PromptDialog.Text(context, ResumeAfterVerificationCode, "Please provide your JIRA verification code", attempts: 1);
            }
            catch (System.IO.FileNotFoundException fex)
            {
                await context.PostAsync($"Could not find file '{fex.FileName}' with exception ${fex}");
            }
            catch (Exception ex)
            {
                await context.PostAsync($"Failed setting up OAUTH with message: {ex}");
            }
        }

        private async Task ShowSignInCard(IDialogContext context, Activity activity, string authorizationLink)
        {
            Activity signInCard = activity.CreateReply();
            signInCard.Recipient = activity.From;
            signInCard.Type = "message";
            signInCard.Attachments = new List<Attachment>();
            List<CardAction> cardButtons = new List<CardAction>();
            CardAction plButton = new CardAction()
            {
                Value = authorizationLink,
                Type = "signin",
                Title = "Connect"
            };
            cardButtons.Add(plButton);
            SigninCard plCard = new SigninCard(text: "You need to authorize me", buttons: cardButtons);
            Attachment plAttachment = plCard.ToAttachment();
            signInCard.Attachments.Add(plAttachment);

            await context.PostAsync(signInCard);
        }

        private async Task ResumeAfterVerificationCode(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var verificationCode = await result;

                if (verificationCode == null || verificationCode.Length != 6)
                {
                    await context.PostAsync("Verification code is not correct (facepalm). Please try **login** again");
                    return;
                }

                var oauthToken = context.UserData.Get<string>(UserDataKeys.OAuthTokenKey);
                var oauthTokenSecret = context.UserData.Get<string>(UserDataKeys.OAuthTokenSecretKey);

                await context.PostAsync("(clap)(clap)(clap) You are now authenticated with JIRA!");

                var request = new RestRequest(JiraProvider.OAuthAccessUrl, Method.POST);
                var oauth1Authenticator = OAuth1Authenticator.ForAccessToken(consumerKey, consumerSecret, oauthToken,
                  oauthTokenSecret, verificationCode);
                oauth1Authenticator.SignatureMethod = RestSharp.Authenticators.OAuth.OAuthSignatureMethod.RsaSha1;

                RestClient client = new RestClient(url)
                {
                    Authenticator = oauth1Authenticator
                };

                var response = await client.ExecutePostTaskAsync(request);

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new InvalidOperationException($"Could not get request access token from {request.Resource}. Response: {response.Content}");
                }

                var qs = HttpUtility.ParseQueryString(response.Content);
                oauthToken = qs["oauth_token"];
                oauthTokenSecret = qs["oauth_token_secret"];

                context.UserData.SetValue(UserDataKeys.OAuthTokenKey, oauthToken);
                context.UserData.SetValue(UserDataKeys.OAuthTokenSecretKey, oauthTokenSecret);

                _jiraProvider.SetOAuthData(consumerKey, consumerSecret, oauthToken, oauthTokenSecret);
            }
            catch (System.IO.FileNotFoundException fex)
            {
                await context.PostAsync($"Could not find file '{fex.FileName}' with exception ${fex}");
            }
            catch (Exception ex)
            {
                await context.PostAsync($"Failed with message: {ex}");
            }
            finally
            {
                context.Done(new object());
            }
        }
    }
}
