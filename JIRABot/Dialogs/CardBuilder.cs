﻿using System;
using System.Collections.Generic;
using Atlassian.Jira;
using Microsoft.Bot.Connector;

namespace JIRABot
{
    public static class CardBuilder
    {
        public static Microsoft.Bot.Connector.Attachment BuildJiraCard(Issue issue)
        {
            UriBuilder issueUrl = new UriBuilder(issue.Jira.Url)
            {
                // https://raleighrocks.atlassian.net/browse/JB-30
                Path = @"browse/" + issue.Key.Value,
                Port = -1,
            };

            CardAction tapAction = new CardAction
            {
                Value = issueUrl.Uri.ToString(),
                Type = "openURL"
            };

            List<CardImage> cardImages = new List<CardImage>
                {
                new CardImage(Resource.ResourceManager.GetString(issue.Type.Name.ToUpper().Replace(" ", string.Empty))),
            };

            List<CardAction> cardButtons = new List<CardAction>();

            CardAction plButton = new CardAction
            {
                Value = issueUrl.Uri.ToString(),
                Type = "openUrl",
                Title = "View"
            };
            cardButtons.Add(plButton);

            //Truncate verbose descriptions for presentation purposes
            int summaryPadding = issue.Summary.Length > BotConfiguration.MaxSummaryDisplayLength
                ? 0
                : BotConfiguration.MaxSummaryDisplayLength - issue.Summary.Length;
            string summary = issue.Summary.Substring(0,
                        issue.Summary.Length > BotConfiguration.MaxSummaryDisplayLength
                            ? BotConfiguration.MaxSummaryDisplayLength
                            : issue.Summary.Length) +
                                  (issue.Summary.Length > BotConfiguration.MaxSummaryDisplayLength ? " . . ." : "").PadRight(summaryPadding);

            string description = $"<b>Priority</b>: {issue.Priority.Name}\n<b>Assignee</b>: {issue.Assignee ?? "Unassigned"}\n<b>Status</b>: {issue.Status.Name}";
            //if (issue.Description != null)
            //{
            //    int descriptionPadding = issue.Description.Length > BotConfiguration.MaxDescriptionDisplayLength
            //        ? 0
            //        : BotConfiguration.MaxDescriptionDisplayLength - issue.Description.Length;

            //    description += issue.Description.Substring(0,
            //        issue.Description.Length > BotConfiguration.MaxDescriptionDisplayLength
            //            ? BotConfiguration.MaxDescriptionDisplayLength
            //            : issue.Description.Length) +
            //                         (issue.Description.Length > BotConfiguration.MaxDescriptionDisplayLength
            //                             ? " . . ."
            //                             : "").PadRight(
            //                                 descriptionPadding);
            //}

            ThumbnailCard plCard = new ThumbnailCard
            {
                Title = issue.Key.Value,
                Subtitle = summary,
                Images = cardImages,
                Buttons = cardButtons,
                Text = description,
                Tap = tapAction
            };

            return plCard.ToAttachment();
        }

        public static Microsoft.Bot.Connector.Attachment BuildJiraCard(JiraItem issue)
        {
            var issueUrl = issue.ToUrl();

            CardAction tapAction = new CardAction
            {
                Value = issueUrl,
                Type = "openURL"
            };

            List<CardImage> cardImages = new List<CardImage>
                {
                new CardImage(Resource.ResourceManager.GetString(issue.Type.ToString().ToUpper().Replace(" ", string.Empty))),
            };

            List<CardAction> cardButtons = new List<CardAction>();

            CardAction plButton = new CardAction
            {
                Value = issueUrl,
                Type = "openUrl",
                Title = "Create"
            };
            cardButtons.Add(plButton);

            //Truncate verbose descriptions for presentation purposes
            int summaryPadding = issue.Summary.Length > BotConfiguration.MaxSummaryDisplayLength
                ? 0
                : BotConfiguration.MaxSummaryDisplayLength - issue.Summary.Length;
            string summary = issue.Summary.Substring(0,
                        issue.Summary.Length > BotConfiguration.MaxSummaryDisplayLength
                            ? BotConfiguration.MaxSummaryDisplayLength
                            : issue.Summary.Length) +
                                  (issue.Summary.Length > BotConfiguration.MaxSummaryDisplayLength ? " . . ." : "").PadRight(summaryPadding);

            string description = "<b>Priority</b>: " + issue.Priority + "\n";
            if (issue.Description != null)
            {
                int descriptionPadding = issue.Description.Length > BotConfiguration.MaxDescriptionDisplayLength
                    ? 0
                    : BotConfiguration.MaxDescriptionDisplayLength - issue.Description.Length;

                description += issue.Description.Substring(0,
                    issue.Description.Length > BotConfiguration.MaxDescriptionDisplayLength
                        ? BotConfiguration.MaxDescriptionDisplayLength
                        : issue.Description.Length) +
                                     (issue.Description.Length > BotConfiguration.MaxDescriptionDisplayLength
                                         ? " . . ."
                                         : "").PadRight(
                                             descriptionPadding);
            }

            ThumbnailCard plCard = new ThumbnailCard
            {
                Title = "NEW",
                Subtitle = summary,
                Images = cardImages,
                Buttons = cardButtons,
                Text = description,
                Tap = tapAction
            };

            return plCard.ToAttachment();
        }
    }
}
