﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;
using JIRABot.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Internals.Fibers;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;

namespace JIRABot.Dialogs
{
    [LuisModel("21469e40-5a20-4e8b-9415-3d7c1b1f9021", "4b1d0c291f5747cea115d30926304480")]
    [Serializable]
    public class JiraDialog : LuisDialog<object>
    {
        readonly IJiraProvider _jiraProvider;
        readonly LoginDialog _loginDialog;

        public JiraDialog(IJiraProvider jiraProvider, LoginDialog loginDialog)
        {
            SetField.NotNull(out _jiraProvider, nameof(_jiraProvider), jiraProvider);
            SetField.NotNull(out _loginDialog, nameof(_loginDialog), loginDialog);
        }

        [LuisIntent("")]
        public async Task None(IDialogContext context, LuisResult result)
        {
            string nickname;

            if (!context.UserData.TryGetValue(UserDataKeys.NicknameKey, out nickname))
            {
                PromptDialog.Text(context, ResumeAfterNickname, "Before we get started, how shall I address you?");
                return;
            }

            await context.PostAsync($"I'm sorry, {nickname}, I didn't understand you. \n\n Type **help** to learn how to interact with me.");

            context.Wait(MessageReceived);
        }

        [LuisIntent("ShowItem")]
        public async Task ShowItems(IDialogContext context, LuisResult result)
        {
            using (var jiraAuthCheck = new JiraAuthorizationCheck(context, _jiraProvider, MessageReceived))
            {
                if (!jiraAuthCheck.IsUserAuthorizedWithJira)
                    return;

                if (result.Entities.Any(e => e.Type == "JiraItem"))
                {
                    await HandleShowItems(context, result, null);
                }
                else
                {
                    var itemFilters = result.GetJiraStatus();

                    if (itemFilters.Count > 0)
                    {
                        var issues = await _jiraProvider.GetIssuesByJql(QueryHelper.CreateStatusQuery(itemFilters));

                        var heading = $"Showing {string.Join(", ", itemFilters)} items";
                        await ShowCardCarousel(context, issues, heading);
                    }
                    else
                    {
                        await context.PostAsync("No item filter specified; nothing to show");
                    }
                }
            }
        }

        [LuisIntent("ShowMyItem")]
        public async Task ShowMyItems(IDialogContext context, LuisResult result)
        {
            using (var jiraAuthCheck = new JiraAuthorizationCheck(context, _jiraProvider, MessageReceived))
            {
                if (!jiraAuthCheck.IsUserAuthorizedWithJira) return;
                var itemFilters = result.GetJiraStatus();

                // note: item filters may be an empty collection, in which case it will show all items assigned to the current user
                var issues = await _jiraProvider.GetIssuesByJql(
                    QueryHelper.JoinQueryAnd(
                        QueryHelper.CreateStatusQuery(itemFilters),
                        QueryHelper.CreateCurrentUserQuery()
                        ) + " ORDER BY createdDate DESC");

                var heading = $"Showing your {string.Join(", ", itemFilters)} items";
                await ShowCardCarousel(context, issues, heading);
            }
        }

        [LuisIntent("LogTime")]
        public async Task LogTime(IDialogContext context, LuisResult result)
        {
            using (var jiraAuthCheck = new JiraAuthorizationCheck(context, _jiraProvider, MessageReceived))
            {
                if (!jiraAuthCheck.IsUserAuthorizedWithJira) return;

                string message;

                var time = result.Entities.FirstOrDefault(e => e.Type == "JiraTime")?.Entity;
                var key = result.ExtractJiraItems().FirstOrDefault();

                if (key != null && time != null)
                {
                    message = $"Logging {time} to {key}";
                    await _jiraProvider.LogTimeToIssue(key, time);
                }
                else
                {
                    message = $"Could not log time, missing {(key == null ? "key" : "time")}";
                }

                await context.PostAsync(message);
            }
        }

        [LuisIntent("LogMeIn")]
        public async Task LogMeIn(IDialogContext context, LuisResult result)
        {
            try
            {
                await context.PostAsync("Logging you in!");
                await context.Forward(_loginDialog, ResumeAfterLoginDialog, context.Activity, System.Threading.CancellationToken.None);
            }
            catch (Exception ex)
            {
                context.Fail(ex);
            }
        }

        private async Task ResumeAfterLoginDialog(IDialogContext context, IAwaitable<object> result)
        {
            try
            {
                var message = await result;
            }
            catch (Exception ex)
            {
                await context.PostAsync($"Failed with message: {ex.Message}");
            }
            finally
            {
                context.Wait(MessageReceived);
            }
        }

        [LuisIntent("ChangeNickname")]
        public async Task ChangeNickname(IDialogContext context, LuisResult result)
        {
            string nickname;
            if (context.UserData.TryGetValue(UserDataKeys.NicknameKey, out nickname))
            {
                PromptDialog.Text(context, ResumeAfterNickname,
                    $"If you're not {nickname} then how shall I call you from now on?");
                return;
            }

            PromptDialog.Text(context, ResumeAfterNickname, "How shall I call you?");
        }

        [LuisIntent("CreateItem")]
        public async Task CreateItem(IDialogContext context, LuisResult result)
        {
            var item = new JiraItem();
            int currIndex = 0;

            TypeOptions type;
            if (Enum.TryParse(result.GetItemType(), out type))
            {
                item.Type = type;
                currIndex = result.GetMaxEntityIndex(LuisConstants.JiraItemType, currIndex);
            }

            PriorityOptions priority;
            if (Enum.TryParse(result.GetItemPriority(), out priority))
            {
                item.Priority = priority;
                currIndex = result.GetMaxEntityIndex(LuisConstants.JiraItemPriority, currIndex);
            }

            item.Summary = result.GetSummary(currIndex).Trim();

            var jiraItemDialog = new FormDialog<JiraItem>(item, JiraItem.BuildForm, FormOptions.PromptInStart);

            context.Call(jiraItemDialog, CreateJiraItemComplete);
        }

        [LuisIntent("CheckTime")]
        public async Task CheckTime(IDialogContext context, LuisResult result)
        {
            using (var jiraAuthCheck = new JiraAuthorizationCheck(context, _jiraProvider, null))
            {
                if (!jiraAuthCheck.IsUserAuthorizedWithJira) return;

                var issueWorklogs = await RetrieveIssueWorklogs();

                string missingDayStatement = GetMissingDaysStatement(issueWorklogs, DateTime.Now);
                if (!string.IsNullOrEmpty(missingDayStatement))
                {
                    await context.PostAsync(string.Join(Environment.NewLine + Environment.NewLine, missingDayStatement,
                        GetFormattedLog(issueWorklogs, DateTime.Now)));
                }
                else
                {
                    await context.PostAsync("Your time log is up to date!");
                }
            }
        }

        [LuisIntent("ShowTime")]
        public async Task ShowTime(IDialogContext context, LuisResult result)
        {
            using (var jiraAuthCheck = new JiraAuthorizationCheck(context, _jiraProvider, null))
            {
                if (!jiraAuthCheck.IsUserAuthorizedWithJira) return;

                var issueWorklogs = await RetrieveIssueWorklogs();

                await context.PostAsync(GetFormattedLog(issueWorklogs, DateTime.Now));
            }
        }

        [LuisIntent("Greeting")]
        public async Task Greeting(IDialogContext context, LuisResult result)
        {
            await context.PostAsync(Messages.Greeting);
        }

        private async Task CreateJiraItemComplete(IDialogContext context, IAwaitable<JiraItem> result)
        {
            JiraItem jiraItem = null;
            try
            {
                jiraItem = await result;
            }
            catch (OperationCanceledException)
            {
                await context.PostAsync("JIRA item not created.");
                return;
            }

            if (jiraItem != null)
            {
                IMessageActivity replyToConversation = context.MakeMessage();
                replyToConversation.Recipient = context.Activity.From;
                replyToConversation.Type = "message";
                replyToConversation.Attachments = new List<Microsoft.Bot.Connector.Attachment>();

                var plCard = CardBuilder.BuildJiraCard(jiraItem);
                replyToConversation.Attachments.Add(plCard);

                replyToConversation.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                await context.PostAsync(replyToConversation);
            }
            else
            {
                await context.PostAsync("Form returned empty response.");
            }

            context.Wait(MessageReceived);
        }

        private async Task ResumeAfterNickname(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                string nickname = await result;

                await context.PostAsync($"Welcome {nickname}! Pleased to meet you! \n\n Type **help** to learn how to interact with me.");

                context.UserData.SetValue(UserDataKeys.NicknameKey, nickname);
            }
            catch (Exception ex)
            {
                await context.PostAsync($"Failed with message: {ex.Message}");
            }
            finally
            {
                context.Wait(MessageReceived);
            }
        }

        #region Worklog handling

        private async Task<List<Tuple<Issue, List<Worklog>>>> RetrieveIssueWorklogs()
        {
            // get all the issues that have been worked on this week
            var issues = await _jiraProvider.GetIssuesByJql(@"worklogDate > startOfWeek() AND worklogAuthor = currentUser()");
            var issueWorklogs = new List<Tuple<Issue, List<Worklog>>>();

            // get all the time from all the worklogs
            foreach (var issue in issues)
            {
                var logs = (await issue.GetWorklogsAsync()).ToList();
                if (logs.Count > 0)
                {
                    issueWorklogs.Add(new Tuple<Issue, List<Worklog>>(issue, logs));
                }
            }
            return issueWorklogs;
        }

        private static long SumLogsThisWeek(IEnumerable<Worklog> logs)
        {
            long result = 0;
            foreach (var log in logs)
            {
                // is the log within the timespan that we care about?
                if (log.StartDate >= DateTime.Today.Subtract(new TimeSpan((int)DateTime.Now.DayOfWeek, 0, 0, 0)))
                {
                    result += log.TimeSpentInSeconds;
                }
            }

            return result;
        }

        private static long SumLogsThisDay(DayOfWeek day, IEnumerable<Worklog> logs, DateTime date)
        {
            long result = 0;
            foreach (var log in logs)
            {
                var dateOfWeek = date.Date.Subtract(new TimeSpan((int)date.DayOfWeek - (int)day, 0, 0, 0));
                // is the log within the timespan that we care about?
                if (log.StartDate >= dateOfWeek && log.StartDate < dateOfWeek.AddDays(1))
                {
                    result += log.TimeSpentInSeconds;
                }
            }

            return result;
        }

        private static double GetHours(long seconds)
        {
            return Math.Round(seconds / 60.0 / 60.0, 1);
        }

        private static string GetFormattedLog(List<Tuple<Issue, List<Worklog>>> issueLogs, DateTime date)
        {
            var result = new StringBuilder();
            foreach (var day in Enumerable.Range(0, 7).Select(x => (DayOfWeek)x))
            {
                var dayReport = new StringBuilder();
                foreach (var item in issueLogs)
                {
                    var hoursForIssue = GetHours(SumLogsThisDay(day, item.Item2, date));
                    if (hoursForIssue > 0.01)
                    {
                        dayReport.Append($"{item.Item1.Key}: {hoursForIssue:0.#}h{Environment.NewLine}{Environment.NewLine}");
                    }
                }

                if (dayReport.Length > 0)
                {
                    dayReport.Insert(0, $"{Enum.GetName(typeof(DayOfWeek), day)}{Environment.NewLine}{Environment.NewLine}");
                    result.Append(dayReport);
                }
            }

            return result.ToString();
        }

        private static string GetMissingDaysStatement(List<Tuple<Issue, List<Worklog>>> issueLogs, DateTime date)
        {
            List<Worklog> worklogs = issueLogs.SelectMany(x => x.Item2).ToList();

            var missingDays = new List<string>();
            foreach (DayOfWeek day in Enumerable.Range(1, (int)DateTime.Now.DayOfWeek).Select(x => (DayOfWeek)x))
            {
                double hours = GetHours(SumLogsThisDay(day, worklogs, date));
                var missingHours = 8 - hours;
                if (missingHours > 0.01)
                {
                    missingDays.Add($"{missingHours:0.#}h on {Enum.GetName(typeof(DayOfWeek), day)}");
                }
            }

            return missingDays.Count > 0 ? $"You are missing {string.Join("; ", missingDays)}" : string.Empty;
        }

        #endregion

        #region Display Handlers

        private async Task HandleShowItems(IDialogContext context, LuisResult result, string heading)
        {
            if (result.Entities.Any(e => e.Type == "JiraItem"))
            {
                // LUIS includes spaces in the JiraItem entities because of the way it parses "JB-01"; it also does everything in lower-case
                var keys = result.ExtractJiraItems();


                if (keys.All(key => key.StartsWith("JB", StringComparison.CurrentCulture)))
                {
                    var issues = await _jiraProvider.GetIssues(keys);

                    if (issues != null && issues.Count > 0)
                    {
                        await ShowCardCarousel(context, issues.Values, heading);
                    }
                    else
                    {
                        context.Fail(new ArgumentException($"Could not get data for {keys[0]}"));
                    }
                }
                else
                {
                    var jiraKeys = keys.Aggregate((arg1, arg2) => arg1 + ", " + arg2);
                    await context.PostAsync($"Your jira keys are: {jiraKeys}");
                }
            }
        }

        /// <summary>
        /// Used to build a message for a list of issues; the heading is prepended to the message
        /// </summary>
        private static IMessageActivity BuildMessage(IDialogContext context, IEnumerable<Issue> issues,
            string heading = null)
        {
            StringBuilder message = new StringBuilder();

            if (heading != null)
            {
                message.Append($"{heading}{Environment.NewLine}{Environment.NewLine}");
            }

            foreach (var issue in issues)
            {
                message.Append($"  {issue.Key}: {issue.Summary} {Environment.NewLine}{Environment.NewLine}");
            }

            var result = context.MakeMessage();
            result.Text = message.ToString();

            return result;
        }

        private async Task ShowCardCarousel(IDialogContext context, ICollection<Issue> issues, string heading)
        {
            if (issues.Count == 0)
            {
                await context.PostAsync((heading != null ? heading + Environment.NewLine + Environment.NewLine: string.Empty) + "No items to show");
            }
            else if (issues.Count == 1)
            {
                await ShowSingleCard(context, issues.First(), heading);
            }
            else
            {
                IMessageActivity replyToConversation = context.MakeMessage();
                replyToConversation.Recipient = context.Activity.From;
                replyToConversation.Type = "message";
                replyToConversation.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                replyToConversation.Attachments = new List<Microsoft.Bot.Connector.Attachment>();
                if (heading != null)
                {
                    replyToConversation.Text = heading;
                }

                foreach (Issue issue in issues.Take(5))
                {
                    var card = CardBuilder.BuildJiraCard(issue);
                    replyToConversation.Attachments.Add(card);
                }

                replyToConversation.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                await context.PostAsync(replyToConversation);
            }
        }

        private async Task ShowSingleCard(IDialogContext context, Issue issue, string heading)
        {
            IMessageActivity replyToConversation = context.MakeMessage();
            replyToConversation.Recipient = context.Activity.From;
            replyToConversation.Type = "message";
            replyToConversation.Attachments = new List<Microsoft.Bot.Connector.Attachment>();
            if (heading != null)
            {
                replyToConversation.Text = heading;
            }

            var plCard = CardBuilder.BuildJiraCard(issue);
            replyToConversation.Attachments.Add(plCard);

            replyToConversation.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            await context.PostAsync(replyToConversation);
        }

        //private static HeroCard CreateThumbnailCard(Issue issue)
        //{
        //    UriBuilder issueUrl = new UriBuilder(issue.Jira.Url)
        //    {
        //        // https://raleighrocks.atlassian.net/browse/JB-30
        //        Path = @"browse/" + issue.Key.Value,
        //        Port = -1,
        //    };

        //    CardAction tapAction = new CardAction
        //    {
        //        Value = issueUrl.Uri.ToString(),
        //        Type = "openURL"
        //    };

        //    List<CardImage> cardImages = new List<CardImage>
        //    {
        //        new CardImage(Resource.ResourceManager.GetString(issue.Type.Name.ToUpper().Replace(" ", string.Empty))),
        //    };

        //    List<CardAction> cardButtons = new List<CardAction>();

        //    CardAction plButton = new CardAction
        //    {
        //        Value = issueUrl.Uri.ToString(),
        //        Type = "openUrl",
        //        Title = "View"
        //    };
        //    cardButtons.Add(plButton);

        //    //Truncate verbose descriptions for presentation purposes
        //    int summaryPadding = issue.Summary.Length > BotConfiguration.MaxSummaryDisplayLength
        //        ? 0
        //        : BotConfiguration.MaxSummaryDisplayLength - issue.Summary.Length;

        //    string description = string.Empty;
        //    if (issue.Description != null)
        //    {
        //        int descriptionPadding = issue.Description.Length > BotConfiguration.MaxDescriptionDisplayLength
        //            ? 0
        //            : BotConfiguration.MaxDescriptionDisplayLength - issue.Description.Length;

        //        description = issue.Description.Substring(0,
        //            issue.Description.Length > BotConfiguration.MaxDescriptionDisplayLength
        //                ? BotConfiguration.MaxDescriptionDisplayLength
        //                : issue.Description.Length) +
        //                             (issue.Description.Length > BotConfiguration.MaxDescriptionDisplayLength
        //                                 ? " . . ."
        //                                 : "").PadRight(
        //                                     descriptionPadding);
        //    }

        //    HeroCard plCard = new HeroCard
        //    {
        //        Title = issue.Key.Value,
        //        Subtitle =
        //            issue.Summary.Substring(0,
        //                issue.Summary.Length > BotConfiguration.MaxSummaryDisplayLength
        //                    ? BotConfiguration.MaxSummaryDisplayLength
        //                    : issue.Summary.Length) +
        //            (issue.Summary.Length > BotConfiguration.MaxSummaryDisplayLength ? " . . ." : "").PadRight(summaryPadding),
        //        Images = cardImages,
        //        Buttons = cardButtons,
        //        Text = description,
        //        Tap = tapAction
        //    };
        //    return plCard;
        //}

        #endregion
    }
}
