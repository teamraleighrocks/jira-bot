﻿using System;
using JIRABot.JIRA;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Internals.Fibers;

namespace JIRABot
{
    public class JiraAuthorizationCheck : IDisposable
    {
        private readonly ResumeAfter<IMessageActivity> _resume;
        private readonly IDialogContext _context;
        private readonly IJiraProvider _jiraProvider;

        public JiraAuthorizationCheck(IDialogContext context, IJiraProvider jiraProvider, ResumeAfter<IMessageActivity> resume)
        {
            _resume = resume;
            SetField.NotNull(out this._context, nameof(_context), context);
            SetField.NotNull(out this._jiraProvider, nameof(_jiraProvider), jiraProvider);
         
            string oauthToken, oauthTokenSecret;

            if (!jiraProvider.IsAuthenticated
              && context.UserData.TryGetValue(UserDataKeys.OAuthTokenKey, out oauthToken)
              && context.UserData.TryGetValue(UserDataKeys.OAuthTokenSecretKey, out oauthTokenSecret))
            {
                string consumerKey = BotConfiguration.OAuthConsumerKey;
                string consumerSecret = BotConfiguration.OAuthConsumerSecret;

                jiraProvider.SetOAuthData(consumerKey, consumerSecret, oauthToken, oauthTokenSecret);
            }

            // still not authenticated
            if(!jiraProvider.IsAuthenticated)
            {
                context.PostAsync("Hey (wave)! Please login to JIRA using **login** command.");
            }
        }

        public bool IsUserAuthorizedWithJira => _jiraProvider.IsAuthenticated;

        public void Dispose()
        {
            if(_resume != null)
            {
                _context.Wait(_resume);
            }
        }
    }
}
