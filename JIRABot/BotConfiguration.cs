﻿using System.ComponentModel;
using System.Configuration;

namespace JIRABot
{
    public class BotConfiguration
    {
        public static bool ShowExceptions => GetFromAppSettings<bool>("ShowExceptions");
        public static int StackTraceLength => GetFromAppSettings<int>("StackTraceLength");
        public static string JiraUrl => GetFromAppSettings<string>("Jira.Url");
        public static string OAuthConsumerKey => GetFromAppSettings<string>("OAuth.ConsumerKey");
        public static string OAuthConsumerSecret => GetFromAppSettings<string>("OAuth.ConsumerSecret");
        public static int MaxDescriptionDisplayLength => GetFromAppSettings<int>("MaxDescriptionDisplayLength");
        public static int MaxSummaryDisplayLength => GetFromAppSettings<int>("MaxSummaryDisplayLength");
        public static int MaxCardsInSkypeCarousel => GetFromAppSettings<int>("MaxCardsInSkypeCarousel");
        
        private static T GetFromAppSettings<T>(string key)
        {
            if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings[key]))
                return default(T);

            T value;
            try
            {
                value = (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(ConfigurationManager.AppSettings[key]);
            }
            catch
            {
                return default(T);
            }

            return value;
        }
    }
}