﻿namespace JIRABot
{
    public class Messages
    {
        private const string NewLine = "\n\n";
        private const string LineBreak = "\n\n \u205f \n\n";

        public static string Greeting =
            $"Hi, I'm **jBot**! {LineBreak}" +
            "I can help you work with your *JIRA*. " +
            "You can use me to review existing tasks, create new ones, log or check time spent on a given issue and other actions typical " +
            $"to managing projects. {LineBreak}" +
            $"Here are some of the ways in which you can interact with me: {LineBreak}" +

            $"- **log in** {NewLine}" +
            $"Allows you to authenticate with JIRA. {LineBreak}" +

            $"- **show items {{type}}** {NewLine}" +
            $"Shows all items of a given type. {NewLine}*example: show items in progress* {LineBreak}" +

            $"- **show my [type] items** {NewLine}" +
            $"Shows all items assigned to you. Type parameter is optional. {NewLine}*example: show my items* {LineBreak}" +

            $"- **show time** {NewLine}" +
            $"Displays time logged this week. {LineBreak}" +

            $"- **check time** {NewLine}" +
            $"Checks if user has logged 8h per day this week. {LineBreak}" +

            $"- **log [time] [to item]** {NewLine}" +
            $"Allows you to log time spent working on a given item. {NewLine}*example: log 2h to JB-2* {LineBreak}" +

            $"- **create [priority] [task] [description]** {NewLine}" +
            $"Allows you create a link to generate a new JIRA item. {NewLine}*example: create P2 epic there's still much to be done* {LineBreak}" +

            $"- **change name** {NewLine}" +
            $"Allows you to change the name which I'm addressing you by. {LineBreak}";

        public static string Goodbye =
            $"Sorry to see you go :( {LineBreak}" +
            "Come back soon!";

        public static string GoodbyeBot =
            $"I'm so sad you want me to leave :( {LineBreak}" +
            "See you soon!";
    }
}