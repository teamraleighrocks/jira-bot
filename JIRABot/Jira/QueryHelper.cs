using System;
using System.Collections.Generic;
using System.Linq;

namespace JIRABot
{
    internal static class QueryHelper
    {
        /// <summary>
        /// Returns a JIRA query containing a status constraint for the statuses, or an empty string
        /// </summary>
        public static string CreateStatusQuery(ICollection<string> status)
        {
            var escapedStatuses = status.Select(s => s.Contains(" ") ? "\'" + s + "\'" : s);

            return status.Count > 0 ? $"status IN ( {string.Join(", ", escapedStatuses)} )" : string.Empty;
        }

        public static string CreateCurrentUserQuery()
        {
            return "assignee = currentUser()";
        }

        /// <summary>
        /// Joins query segments, ignores empyy segments
        /// </summary>
        public static string JoinQuery(JoinMethod method, params string[] segments)
        {
            string joiner = method == JoinMethod.And ? "AND" : "OR";
            return string.Join($" {joiner} ", segments.Where(s => s.Length > 0));
        }

        public static string JoinQueryAnd(params string[] segments)
        {
            return JoinQuery(JoinMethod.And, segments);
        }
    }
}