﻿using System.Net;
using System.Text;

namespace JIRABot
{
    public static class JiraItemConverters
    {
        private const string _jiraBotProjectId = "10000";

        public static string ToUrl(this JiraItem item)
        {
            if (item == null)
                return null;

            StringBuilder urlBuilder = new StringBuilder();
            urlBuilder.Append(BotConfiguration.JiraUrl + "/secure/CreateIssueDetails!init.jspa?");
            urlBuilder.Append(CreateQueryParam("pid", _jiraBotProjectId).TrimStart('&'));
            urlBuilder.Append(CreateQueryParam("priority", (item.Priority != null ? (int)item.Priority : (int)PriorityOptions.Medium).ToString()));
            urlBuilder.Append(CreateQueryParam("issuetype", (item.Type != null ? (int)item.Type : (int)TypeOptions.Task).ToString()));
            urlBuilder.Append(CreateQueryParam("summary", WebUtility.UrlEncode(item.Summary)));
            urlBuilder.Append(CreateQueryParam("description", WebUtility.UrlEncode(item.Description)));
            urlBuilder.Append(CreateQueryParam("assignee", WebUtility.UrlEncode(item.Asignee)));

            return urlBuilder.ToString();
        }

        private static string CreateQueryParam(string name, string value)
        {
            return $"&{name}={value}";
        }
    }
}