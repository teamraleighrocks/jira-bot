﻿using Microsoft.Bot.Builder.Calling;
using Microsoft.Bot.Builder.Calling.ObjectModel.Misc;
using System;
using System.Collections.Generic;
using Microsoft.Bot.Builder.Calling.Events;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Calling.ObjectModel.Contracts;

namespace JIRABot
{
    public class JiraCallingBot : IDisposable, ICallingBot
    {
        private const string NewJiraItem = "1";
        private const string MoreAboutJiraBot = "2";
        private const string Other = "3";

        private const string NewTask = "1";
        private const string NewImprovement = "2";
        private const string NewFeature = "3";
        private const string NewBug = "4";
        private const string NewEpic = "5";
        private const string NewInvestigation = "6";

        private readonly Dictionary<string, CallState> _callStateMap = new Dictionary<string, CallState>();

        public ICallingBotService CallingBotService { get; private set; }

        public JiraCallingBot(ICallingBotService callingBotService)
        {
            if (callingBotService == null)
                throw new ArgumentNullException(nameof(callingBotService));

            CallingBotService = callingBotService;

            CallingBotService.OnIncomingCallReceived += OnIncomingCallReceived;
            CallingBotService.OnPlayPromptCompleted += OnPlayPromptCompleted;
            CallingBotService.OnRecordCompleted += OnRecordCompleted;
            CallingBotService.OnRecognizeCompleted += OnRecognizeCompleted;
            CallingBotService.OnHangupCompleted += OnHangupCompleted;
        }

        private Task OnIncomingCallReceived(IncomingCallEvent incomingCallEvent)
        {
            var id = Guid.NewGuid().ToString();
            _callStateMap[incomingCallEvent.IncomingCall.Id] = new CallState();
            incomingCallEvent.ResultingWorkflow.Actions = new List<ActionBase>
                {
                    new Answer { OperationId = id },
                    GetPromptForText(IvrOptions.WelcomeMessage)
                };

            return Task.FromResult(true);
        }

        private Task OnPlayPromptCompleted(PlayPromptOutcomeEvent playPromptOutcomeEvent)
        {
            var callStateForClient = _callStateMap[playPromptOutcomeEvent.ConversationResult.Id];
            callStateForClient.InitiallyChosenMenuOption = null;
            SetupInitialMenu(playPromptOutcomeEvent.ResultingWorkflow);

            return Task.FromResult(true);
        }

        private Task OnRecordCompleted(RecordOutcomeEvent recordOutcomeEvent)
        {
            var id = Guid.NewGuid().ToString();
            recordOutcomeEvent.ResultingWorkflow.Actions = new List<ActionBase>
                {
                    GetPromptForText(IvrOptions.Ending),
                    new Hangup { OperationId = id }
                };
            recordOutcomeEvent.ResultingWorkflow.Links = null;
            _callStateMap.Remove(recordOutcomeEvent.ConversationResult.Id);
            return Task.FromResult(true);
        }

        private Task OnRecognizeCompleted(RecognizeOutcomeEvent recognizeOutcomeEvent)
        {
            var callStateForClient = _callStateMap[recognizeOutcomeEvent.ConversationResult.Id];

            switch (callStateForClient.InitiallyChosenMenuOption)
            {
                case null:
                    ProcessMainMenuSelection(recognizeOutcomeEvent, callStateForClient);
                    break;
                case NewJiraItem:
                    ProcessJiraItemSelection(recognizeOutcomeEvent, callStateForClient);
                    break;
                default:
                    SetupInitialMenu(recognizeOutcomeEvent.ResultingWorkflow);
                    break;
            }
            return Task.FromResult(true);
        }

        private Task OnHangupCompleted(HangupOutcomeEvent hangupOutcomeEvent)
        {
            hangupOutcomeEvent.ResultingWorkflow = null;
            return Task.FromResult(true);
        }

        private void SetupInitialMenu(Workflow workflow)
        {
            workflow.Actions = new List<ActionBase> { CreateIvrOptions(IvrOptions.MainMenuPrompt, 3, false) };
        }

        private void ProcessMainMenuSelection(RecognizeOutcomeEvent outcome, CallState callStateForClient)
        {
            if (outcome.RecognizeOutcome.Outcome != Outcome.Success)
            {
                SetupInitialMenu(outcome.ResultingWorkflow);
                return;
            }

            switch (outcome.RecognizeOutcome.ChoiceOutcome.ChoiceName)
            {
                case NewJiraItem:
                    callStateForClient.InitiallyChosenMenuOption = NewJiraItem;
                    outcome.ResultingWorkflow.Actions = new List<ActionBase> { CreateNewJiraItemMenu() };
                    break;
                case MoreAboutJiraBot:
                    callStateForClient.InitiallyChosenMenuOption = MoreAboutJiraBot;
                    outcome.ResultingWorkflow.Actions = new List<ActionBase> { GetPromptForText(IvrOptions.MoreInfoPrompt) };
                    break;
                default:
                    SetupInitialMenu(outcome.ResultingWorkflow);
                    break;
            }
        }

        private void ProcessJiraItemSelection(RecognizeOutcomeEvent outcome, CallState callStateForClient)
        {
            if (outcome.RecognizeOutcome.Outcome != Outcome.Success)
            {
                outcome.ResultingWorkflow.Actions = new List<ActionBase> { CreateNewJiraItemMenu() };
                return;
            }
            switch (outcome.RecognizeOutcome.ChoiceOutcome.ChoiceName)
            {
                case NewTask:
                case NewImprovement:
                case NewFeature:
                case NewBug:
                case NewEpic:
                case NewInvestigation:
                    outcome.ResultingWorkflow.Actions = new List<ActionBase>
                        {
                            GetPromptForText("New JIRA item created.")
                        };
                    break;
                default:
                    callStateForClient.InitiallyChosenMenuOption = null;
                    SetupInitialMenu(outcome.ResultingWorkflow);
                    break;
            }
        }

        private ActionBase CreateNewJiraItemMenu()
        {
            return CreateIvrOptions(IvrOptions.NewJiraItemPrompt, 6, true);
        }

        private static Recognize CreateIvrOptions(string textToBeRead, int numberOfOptions, bool includeBack)
        {
            if (numberOfOptions > 9)
                throw new Exception("too many options specified");

            var id = Guid.NewGuid().ToString();
            var choices = new List<RecognitionOption>();
            for (int i = 1; i <= numberOfOptions; i++)
            {
                choices.Add(new RecognitionOption { Name = Convert.ToString(i), DtmfVariation = (char)('0' + i) });
            }
            if (includeBack)
                choices.Add(new RecognitionOption { Name = "#", DtmfVariation = '#' });
            var recognize = new Recognize
            {
                OperationId = id,
                PlayPrompt = GetPromptForText(textToBeRead),
                BargeInAllowed = true,
                Choices = choices
            };

            return recognize;
        }

        private static PlayPrompt GetPromptForText(string text)
        {
            var prompt = new Prompt { Value = text, Voice = VoiceGender.Male };
            return new PlayPrompt { OperationId = Guid.NewGuid().ToString(), Prompts = new List<Prompt> { prompt } };
        }

        public void Dispose()
        {
            if (CallingBotService != null)
            {
                CallingBotService.OnIncomingCallReceived -= OnIncomingCallReceived;
                CallingBotService.OnPlayPromptCompleted -= OnPlayPromptCompleted;
                CallingBotService.OnRecordCompleted -= OnRecordCompleted;
                CallingBotService.OnRecognizeCompleted -= OnRecognizeCompleted;
                CallingBotService.OnHangupCompleted -= OnHangupCompleted;
            }
        }

        private class CallState
        {
            public string InitiallyChosenMenuOption { get; set; }
        }
    }
}