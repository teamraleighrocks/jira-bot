﻿using Microsoft.Bot.Builder.FormFlow;
using System;

namespace JIRABot
{
    public enum TypeOptions
    {
        [Terms(nameof(Task), "user story", "ticket", MaxPhrase = 5)]
        Task = 10101,
        [Terms(nameof(Improvement), "enhancement", MaxPhrase = 5)]
        Improvement = 10100,
        [Terms(nameof(NewFeature), "change request")]
        NewFeature = 10103,
        [Terms(nameof(Bug), "issue", "problem", MaxPhrase = 3)]
        Bug = 10104,
        Epic = 10000,
        [Terms(nameof(Investigation), "spike")]
        Investigation = 10106
    }

    public enum PriorityOptions
    {
        Highest = 1,
        High = 2,
        Medium = 3,
        Lowest = 4,
        Low = 5
    }

    [Serializable]
    [Template(TemplateUsage.Confirmation, "Ay, ay, Sir!", "Roger that!", "Consider it done.", "Right away, Sir!", "I'm on it!")]
    [Template(TemplateUsage.NotUnderstood, "Sorry mate, can't make head or tail of phrase \"{0}\"!", "Phrase \"{0}\" is beyond me, brother...", "Phrase \"{0}\" is like Chinese arithmetic to me.")]
    [Template(TemplateUsage.EnumSelectOne, "What {&} of JIRA item would you like to create? {||}", ChoiceStyle = ChoiceStyleOptions.PerLine, ChoiceFormat = "{1}")]
    public class JiraItem
    {
        public TypeOptions? Type;

        [Prompt("What {&} would you like this {Type} to be? {||}")]
        public PriorityOptions? Priority;

        [Prompt("What's the summary of this {Type}?")]
        public string Summary;

        [Prompt("What's the description of this {Type}?")]
        public string Description;

        [Prompt("Who should be working on it?")]
        public string Asignee;

        public static IForm<JiraItem> BuildForm()
        {
            return new FormBuilder<JiraItem>()
                    .Message("Let's do it!")
                    .Field(nameof(Type))
                    .Field(nameof(Priority))
                    .Field(nameof(Summary))
                    .Field(nameof(Description))
                    .Field(nameof(Asignee))
                    .Message("That's all, mate. Now sit and wait for your item to be created for ya!")
                    .Build();
        }
    }
}