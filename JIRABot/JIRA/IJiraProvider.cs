﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Atlassian.Jira;

namespace JIRABot
{
    public interface IJiraProvider
    {
        Task<Issue> GetIssue(string key);

        Task<IDictionary<string, Issue>> GetIssues(string[] keys);

        Task<IEnumerable<Project>> GetProjects();

        Task<List<Issue>> GetIssuesByJql(string jql);

        void SetOAuthData(string consumerKey, string consumerSecret, string oauthToken, string oauthTokenSecret);

        bool IsAuthenticated { get; }

        Task<Worklog> LogTimeToIssue(string key, string time);
    }
}
