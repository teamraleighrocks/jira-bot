﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Atlassian.Jira;
using RestSharp.Authenticators;

namespace JIRABot
{
    [Serializable]
    public class JiraProvider : IJiraProvider
    {
        public const string OAuthJiraRequestUrl = "/plugins/servlet/oauth/request-token";
        public const string OAuthUserAuthorizeUrl = "/plugins/servlet/oauth/authorize";
        public const string OAuthAccessUrl = "/plugins/servlet/oauth/access-token";

        readonly static Regex keyRegex = new Regex("((?<!([A-Za-z]{1,10})-?)[A-Z]+-\\d+)");
        readonly string url;
        string consumerKey, consumerSecret, oauthToken, oauthTokenSecret;
        Jira _jira;

        public JiraProvider()
        {
            url = BotConfiguration.JiraUrl;
            _jira = Jira.CreateRestClient(url);
        }

        public Task<Issue> GetIssue(string key)
        {
            return _jira.Issues.GetIssueAsync(key);
        }

        public Task<IDictionary<string, Issue>> GetIssues(string[] keys)
        {
            if (keys.Length > BotConfiguration.MaxCardsInSkypeCarousel)
            {
                throw new ArgumentException($"Too many keys requested. Skype supports only {BotConfiguration.MaxCardsInSkypeCarousel}");
            }

            return _jira.Issues.GetIssuesAsync(keys);
        }

        public async Task<List<Issue>> GetIssuesByJql(string jql)
        {
            var result = await _jira.Issues.GetIssuesFromJqlAsync(jql, maxIssues: BotConfiguration.MaxCardsInSkypeCarousel);
            return result.ToList();
        }

        public Task<IEnumerable<Project>> GetProjects()
        {
            return _jira.Projects.GetProjectsAsync();
        }

        public Task<Worklog> LogTimeToIssue(string key, string time)
        {
            return _jira.Issues.AddWorklogAsync(key, new Worklog(time, DateTime.Now));
        }

        public bool IsAuthenticated => _jira.RestClient.RestSharpClient.Authenticator is OAuth1Authenticator;

        public static string[] GetJiraKeys(string text)
        {
            return keyRegex.Matches(text)
                     .Cast<Match>()
                     .Select(m => m.Groups[0].Value)
                     .ToArray();
        }

        public void SetOAuthData(string consumerKey, string consumerSecret, string oauthToken, string oauthTokenSecret)
        {
            this.consumerKey = consumerKey;
            this.consumerSecret = consumerSecret;
            this.oauthToken = oauthToken;
            this.oauthTokenSecret = oauthTokenSecret;

            SetupOAuth();
        }

        private void SetupOAuth()
        {
            var oauthAuthenticator = OAuth1Authenticator.ForProtectedResource(consumerKey, consumerSecret, oauthToken,
                  oauthTokenSecret);
            oauthAuthenticator.SignatureMethod = RestSharp.Authenticators.OAuth.OAuthSignatureMethod.RsaSha1;
            _jira.RestClient.RestSharpClient.Authenticator = oauthAuthenticator;
        }

    }
}
