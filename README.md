**Title:** JIRA Bot

**One-Line Concept:** Developers best friend - get info about JIRA items and presents them in clean way

**Features:**

* Connects to JIRA and get issues in progress that user is assigned to
* Highlights issues requiring immediate attention
* Presents JIRA items in clean and readable way (JIRA cards)
* Helps with time submissions per issue
* Creates JIRA items on the fly
* Connects to Bitbucket and gets info about Pull Requests